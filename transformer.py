from __future__ import print_function  

import csv
import sys
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import XSD

def load_data(file_path):
    """Load data from a CSV file and return it as a list of dictionaries."""
    with open(file_path, mode='r') as file:
        reader = csv.DictReader(file)
        data = [row for row in reader]
    return data

def create_rdf_graph(data):
    """Create an RDF graph using the SAREF ontology from the provided data."""
    g = Graph()

    # Load the SAREF ontology from the web
    print("Loading SAREF ontology...")
    g.parse("https://saref.etsi.org/core/v3.1.1/saref.ttl", format="turtle")
    print("SAREF ontology loaded.")

    # Define namespaces
    SAREF = Namespace("https://saref.etsi.org/core/")
    EX = Namespace("http://example.org/")

    # Bind namespaces to prefixes
    g.bind("saref", SAREF)
    g.bind("ex", EX)

    # Process only the first 1000 rows (more than this, the code will face memory issue)
    for i, row in enumerate(data[:1000]):
        if i % 10000 == 0:
            print("Processing row {}...".format(i))

        timestamp = row['utc_timestamp']
        
        for column, value in row.items():
            if column not in ['utc_timestamp', 'cet_cest_timestamp', 'interpolated'] and value:
                device_uri = URIRef(EX + "device/" + column)
                g.add((device_uri, RDF.type, SAREF.Device))

                if 'grid_import' in column or 'grid_export' in column or 'pv' in column or 'storage_charge' in column or 'storage_decharge' in column:
                    # Add measurements as a property
                    measurement_uri = URIRef(device_uri + "/measurement/" + timestamp)
                    g.add((measurement_uri, RDF.type, SAREF.Measurement))
                    g.add((measurement_uri, SAREF.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))
                    g.add((measurement_uri, SAREF.isMeasurementOf, device_uri))
                    g.add((measurement_uri, SAREF.hasValue, Literal(value, datatype=XSD.float)))

    print("RDF graph creation complete.")
    return g


def main(file_path):
    """Main function to execute the steps of loading data, creating RDF graph, and serializing it."""
    print("Loading data...")
    data = load_data(file_path)  # Load the data from the CSV file
    print("Data loaded. Number of rows: {}".format(len(data)))

    rdf_graph = create_rdf_graph(data)  # Create the RDF graph from the data

    print("Serializing RDF graph to graph.ttl...")
    rdf_graph.serialize(destination='graph.ttl', format='turtle')  # Serialize and save the RDF graph to a file
    print("RDF graph has been saved to graph.ttl")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python transformer.py path/to/dataset.csv")
        sys.exit(1)
    file_path = sys.argv[1]
    main(file_path)
